from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.email_operator import EmailOperator
from datetime import datetime, timedelta

default_args = {
    'owner': 'ashish',
    'depends_on_past': False,
    'start_date': datetime(2017,7,31),
    'email': ['ashish.rawat@myntra.com', 'aniket.jain@myntra.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 0,
    #'retry_delay': timedelta(minutes=5),
    'queue': 'WORKER1',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

today = datetime.today()
todayStr = today.strftime('%Y%m%d')

createDirectoryCommand = "cd /data/daily_sjit && mkdir -p {0}".format(todayStr)
poDataFile = "/data/daily_sjit/{0}/po_sku_reco.csv".format(todayStr)
mailSubject = "Daily SMART JIT PO Data for {0}".format(todayStr)


dag = DAG(
    'daily_sjit', default_args=default_args, schedule_interval="0 7 * * *")


t1 = BashOperator(task_id='CreateDirectory', bash_command=createDirectoryCommand, dag=dag)

t2 = BashOperator(task_id='DownloadRDFFiles', bash_command='cd /myntra/daily_sjit && python downloadRDFFiles.py', dag=dag)

t3 = BashOperator(task_id='Fetch30DaysData', bash_command='cd /myntra/daily_sjit && python fetch30daysdata.py', dag=dag)
t4 = BashOperator(task_id='Fetch7DaysData', bash_command='cd /myntra/daily_sjit && python fetch7daysdata.py', dag=dag)
t5 = BashOperator(task_id='FetchCIData', bash_command='cd /myntra/daily_sjit && python fetchCIdata.py', dag=dag)
t6 = BashOperator(task_id='FetchPOData', bash_command='cd /myntra/daily_sjit && python fetchPOdata.py', dag=dag)

t7 = BashOperator(task_id='DailyForecast', bash_command='cd /myntra/daily_sjit && Rscript gen_daily_sjit_frcst.R', dag=dag) 

t8 = EmailOperator(dag=dag, task_id="SendMail",to=["ashish.rawat@myntra.com","aniket.jain@myntra.com"], 
     subject=mailSubject, html_content="<h5>This data has been generated through SMART JIT. For any queries, please contact ashish.rawat@myntra.com, aniket.jain@myntra.com</h5>", files=[poDataFile])


t1.set_downstream(t2)
t1.set_downstream(t3)
t1.set_downstream(t4)
t1.set_downstream(t5)
t1.set_downstream(t6)

t2.set_downstream(t7)
t3.set_downstream(t7)
t4.set_downstream(t7)
t5.set_downstream(t7)
t6.set_downstream(t7)

t7.set_downstream(t8)
