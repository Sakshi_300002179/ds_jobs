from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta

default_args = {
    'owner': 'sakshi',
    'depends_on_past': False,
    'start_date': datetime(2017,8, 1),
    'email': ['sakshi.barnwal@myntra.com', 'manu.kothari@myntra.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    #'retry_delay': timedelta(minutes=5),
    'queue': 'WORKER1',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}
dag = DAG(
    'SABRE_Closed_Box', default_args=default_args, schedule_interval="55 4 * * *")

t1 = BashOperator(
    task_id='FilesRemoval',
    bash_command='rm /myntra/data/return_abuse/closed_box/*.gz',
    dag=dag)

t2 = BashOperator(
     task_id='GetDataFromBI',
     bash_command='cd /myntra/src/return_abuse/closed_box && python get_data.py',
     retries=3,
     dag=dag)

t3 =BashOperator(
     task_id='Prediction',
     bash_command='cd /myntra/src/return_abuse/closed_box && python predict.py',
     retries=2,
     dag=dag)

t1.set_downstream(t2);
t2.set_downstream(t3);
