from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta

default_args = {
    'owner': 'sakshi',
    'depends_on_past': False,
    'start_date': datetime(2017,4, 21),
    'email': ['sakshi.barnwal@myntra.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'ram':3072
    #'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}
dag = DAG(
    'aviator', default_args=default_args, schedule_interval="0 * * * *")

t1 = BashOperator(
    task_id='PricingSnapshot',
    bash_command='cd /myntra/Aviator && java  -cp aviator-1.0-SNAPSHOT-jar-with-dependencies.jar:mysql-connector-java-5.1.38.jar com.myntra.jobs.PricingSnapshotCassandraPersistor',
    dag=dag)

t2 = BashOperator(
    task_id='Aviator',
    bash_command='cd /myntra/Aviator && java -Xms6g -Xmx6g -cp aviator-1.0-SNAPSHOT-jar-with-dependencies.jar:mysql-connector-java-5.1.38.jar com.myntra.jobs.AviatorEngine ',
    retries=3,
    dag=dag)

t3 =  BashOperator(
      task_id='Mailer',
      bash_command='cd /myntra/Aviator && java -Xms3g -Xmx5g -cp aviator-1.0-SNAPSHOT-jar-with-dependencies.jar:mysql-connector-java-5.1.38.jar com.myntra.jobs.ComputedSnapshotMailerJob',
      dag=dag)

t4 = BashOperator(
    task_id='Combiner',
    bash_command='cd /data/DailyMetricTracking/validate_csvdir && sudo java -jar SendCSVToValidateNew-0.0.1-SNAPSHOT-jar-with-dependencies.jar',
    queue='WORKER2',
    dag=dag)

t5 = BashOperator(
    task_id='Upload',
    bash_command='cd /data/DailyMetricTracking/validate_csvdir/files && sudo sh upload.sh ',
    retries=3,
    queue='WORKER2',
    dag=dag)

t1.set_downstream(t2);
t2.set_downstream(t3);
t2.set_downstream(t4);
t4.set_downstream(t5);
